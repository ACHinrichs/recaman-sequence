class Recaman:
    def __init__(self, maximum_hops=0, start=0):
        self.visited = {start: 0}
        self.current = start
        self.hops = 0
        self.max_hops=maximum_hops

    def __iter__(self):
        return self
    
    def __next__(self):
        if self.hops >= self.max_hops > 0:
            raise StopIteration()  # signals "the end"
        self.hops=self.hops + 1
        
        if ((self.current - self.hops > 0) and
            (self.current - self.hops not in self.visited)):
            self.current = self.current - self.hops
        else:
            self.current = self.current + self.hops
        self.visited[self.current]= True
        return self.current

for i in Recaman(100):
    print(i)
